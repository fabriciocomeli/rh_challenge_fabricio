class EmployeesController < ApplicationController
  before_action :set_employees, only: %i[edit show update destroy]
  def index
    @employees = Employee.search_field(params[:search])
    @workspaces = Workspace.ordered_work
    @job_roles = JobRole.ordered_job
    @users = User.ordered_user
  end

  def show    
  end
  
  def new
    @employee = Employee.new
  end

  def create
    @employee = Employee.new(employees_params)
    if @employee.save
      redirect_to employees_path, notice: "#{@employee.name} cadastrada(o) com sucesso!"
    else
      flash.now[:alert] = @employee.errors.full_messages.to_sentence
      render :new
    end
  end

  def edit    
  end

  def update
    if @employee.update(employees_params)
      redirect_to employees_path, notice: "#{@employee.name} atualizada(o) com sucesso!"
    else
      flash.now[:alert] = @employee.errors.full_messages.to_sentence
      render :edit
    end
  end

  def destroy
    if @employee.destroy
      redirect_to employees_path, notice: "#{@employee.name} excluída(o) com sucesso!"
    else
      flash.now[:alert] = @employee.errors.full_messages.to_sentence
      render :index
    end
  end
    
  private

  def set_employees
    @employee = Employee.find(params[:id])
  end

  def employees_params
    params.require(:employee).permit(:registration, :name, :date_birth, :birth_town, :birth_state, :workspace_id, :job_role_id, :situation, :genre, 
                                    contacts_attributes: [:id, :phone, :email, :cell_phone, :_destroy])
  end
end
