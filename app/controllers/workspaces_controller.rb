class WorkspacesController < ApplicationController
  before_action :set_workspaces, only: %i[edit update]
  
  def new
    @workspace = Workspace.new
  end

  def create
    @workspace = Workspace.new(workspaces_params)
    if @workspace.save
      redirect_to employees_path, notice: "#{@workspace.title} cadastrada(o) com sucesso!"
    else
      flash.now[:alert] = @workspace.errors.full_messages.to_sentence
      render "new"
    end
  end

  def edit    
  end

  def update
    if @workspace.update(workspaces_params)
      redirect_to employees_path, notice: "#{@workspace.title} atualizado com sucesso!"
    else
      flash.now[:alert] = @workspace.errors.full_messages.to_sentence
      render "edit"
    end
  end

  private

  def set_workspaces
    @workspace = Workspace.find(params[:id])
  end
  

  def workspaces_params
    params.require(:workspace).permit(:title)
  end
  
end
