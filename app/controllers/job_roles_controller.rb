class JobRolesController < ApplicationController
  before_action :set_job_role, only: %i[edit update]

  def new
    @job_role = JobRole.new
  end

  def create
    @job_role = JobRole.new(job_role_params)
    if @job_role.save
      redirect_to employees_path, notice: "#{@job_role.title} cadastrada(o) com sucesso!"
    else
      flash.now[:alert] = @job_role.errors.full_messages.to_sentence
      render :new
    end
  end

  def edit
  end

  def update
    if @job_role.update(job_role_params)
      redirect_to employees_path, notice: "#{@job_role.title} atualizado com sucesso!"
    else
      flash.now[:alert] = @job_role.errors.full_messages.to_sentence
      render :edit
    end
  end

  private

  def set_job_role
    @job_role = JobRole.find(params[:id])
  end

  def job_role_params
    params.require(:job_role).permit(:title)
  end
end
