class Workspace < ApplicationRecord
  validates :title, presence: true
  has_many :employees

  scope :ordered_work, -> { order(id: :asc) }
end
