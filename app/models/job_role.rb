class JobRole < ApplicationRecord
  validates :title, presence: true
  has_many :employees

  scope :ordered_job, -> { order(id: :asc) }
end
