class Employee < ApplicationRecord
  validates :registration, :name, presence: true
  validates :registration, :name, uniqueness: true

  enum situation: { 'Solteiro(a)': 0, 'Casado(a)': 1, 'Divorciado(a)': 2, 'Viúvo(a)': 3 }
  enum genre: { Masculino: 0, Feminino: 1 }

  belongs_to :workspace
  belongs_to :job_role
  has_many :contacts, dependent: :destroy
  accepts_nested_attributes_for :contacts, reject_if: :all_blank, allow_destroy: true

  scope :search_field, -> (search){
    where("name like :field or registration like :field", field: "%#{search}%") if search.present?
  }

  validates_uniqueness_of :workspace_id, :scope => :job_role_id
end
