class Contact < ApplicationRecord
  validates :phone, :email, :cell_phone, presence: true
  validates :phone, :email, :cell_phone, uniqueness: true

  belongs_to :employee
end
