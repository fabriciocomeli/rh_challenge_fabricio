Rails.application.routes.draw do
  devise_for :users
  root 'employees#index'

  resources :job_roles, except: %i[index show destroy]
  resources :workspaces, except: %i[index show destroy]
  resources :employees
  resources :users, exepct: %i[show]
end
