class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :phone
      t.string :email
      t.string :cell_phone
      t.references :employee, foreign_key: true

      t.timestamps
    end
  end
end
