class AddGenreToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :genre, :integer
  end
end
