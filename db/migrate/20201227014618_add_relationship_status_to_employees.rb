class AddRelationshipStatusToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :situation, :integer
  end
end
