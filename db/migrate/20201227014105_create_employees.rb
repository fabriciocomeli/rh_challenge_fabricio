class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :registration
      t.string :name
      t.date :date_birth
      t.string :birth_town
      t.string :birth_state
      t.references :workspace, foreign_key: true
      t.references :job_role, foreign_key: true

      t.timestamps
    end
  end
end
