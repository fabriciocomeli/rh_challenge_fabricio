require "rails_helper"

RSpec.describe Employee do
  it { is_expected.to validate_presence_of(:registration)}
  it { is_expected.to validate_uniqueness_of(:registration)}
  it { is_expected.to validate_presence_of(:name)}
  it { is_expected.to validate_uniqueness_of(:name)}

  it { is_expected.to belong_to :workspace }
  it { is_expected.to belong_to :job_role }
end