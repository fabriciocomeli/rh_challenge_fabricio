require "rails_helper"

RSpec.describe Contact do
  it { is_expected.to validate_presence_of(:phone)}
  it { is_expected.to validate_uniqueness_of(:phone)}
  it { is_expected.to validate_presence_of(:email)}
  it { is_expected.to validate_uniqueness_of(:email)}
  it { is_expected.to validate_presence_of(:cell_phone)}
  it { is_expected.to validate_uniqueness_of(:cell_phone)}

  it { is_expected.to belong_to :employee }
end