FactoryBot.define do
  factory :employee do
    registration { Faker::Number.between(from: 1, to: 10) }
    name { Faker::Name.name }
  end
end