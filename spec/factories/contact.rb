FactoryBot.define do
  factory :contact do
    phone { Faker::PhoneNumber.phone_number }
    email { Faker::Internet.email }
    cell_phone { Faker::PhoneNumber.cell_phone }
  end
end