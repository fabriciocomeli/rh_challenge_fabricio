FactoryBot.define do
  factory :job_role do
    title { Faker::Job.position }
  end
end