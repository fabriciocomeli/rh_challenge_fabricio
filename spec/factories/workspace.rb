FactoryBot.define do
  factory :workspace do
    title { Faker::Job.title }
  end
end