**RH Challenge**
**_Características do projeto:_**
Esse projeto tem como principal característica o cadastro de funcionários para controle da empresa abaixo listo uma das telas e suas funcionalidades.

**Tela de login:**

![image](/uploads/bb2d6b6c2a0436e5781b7f8018c90cae/image.png)

obs: a logo é fictícia

**Tela principal:**

![image](/uploads/2ecd3b09f79f6a963aade86ed5580383/image.png)

aqui é possível visualizar os funcionários ja cadastrados, onde temos 2 opções de busca e botões de editar, visualizar e excluir o cadastro.

**Acesso rápido:**

![image](/uploads/b142626d4860c328b013de9454eb34c4/image.png)

através desse modais é possível realizar a visualização de cada função e com opções de editar e cadastrar.

![image](/uploads/732b9b0c93e145bddb681df0048af7cc/image.png)

**Telas de cadastros:**

![image](/uploads/fb36f1be82c1a889a8765f5ffaef7a3f/image.png)

novo setor

![image](/uploads/5c7a1ba12ecd1f6eb9bbf96324d3193b/image.png)

nova função

![image](/uploads/fb2e09072bffd910df8e462b006ebfae/image.png)

novo usuário(para acessar o painel)

![image](/uploads/6ab7b780015ebeb4a716482a141c19fb/image.png)

novo funcionário, com a opção de cadastrar varios contatos através do nested attributes.

![image](/uploads/0b4e195e0a9a25e77a6dc2800576afa4/image.png)

dados cadastrais do funcionário em detalhes

**Observações:**
Para que o Bootstrap funcione corretamente é necessário ter o Yarn instalado na computador pois utilizei o mesmo pela facilidade de configuração.

Após a configuração executar o comando rake db:seed para gerar o usuário padrão.

link do projeto:
https://rhchallenge.herokuapp.com/

Usuario: admin@admin.com
senha: 123456
